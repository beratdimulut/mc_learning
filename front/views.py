from django.views.generic import ListView, FormView, View
from django.http import HttpResponseRedirect
from .forms import TensorFlowForm
from django.urls import reverse_lazy, reverse
from django.conf import settings

# TensorFlow
from .utils import process_predict, reset_model_save

import shutil
import os


class IndexView(ListView, FormView):
    form_class = TensorFlowForm
    template_name = 'index.html'
    success_url = reverse_lazy('index')
    plus_context = dict()

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['occasionally_loss'] = self.plus_context.get('occasionally_loss', None)
        context['final_rme'] = self.plus_context.get('final_rme', None)
        return context

    def form_valid(self, form):
        train_file = form.cleaned_data['train_file']
        predict_file = form.cleaned_data['predict_file']

        # results = self.process_tensor_flow(train_file, predict_file)
        final_rme, occasionally_loss = process_predict(train_file, predict_file)

        self.plus_context['occasionally_loss'] = occasionally_loss
        self.plus_context['final_rme'] = final_rme

        return super(IndexView, self).form_valid(form)

    def get_queryset(self):
        return []


class ResetView(View):

    def dispatch(self, request, *args, **kwargs):
        reset_model_save()

        return HttpResponseRedirect(reverse('index'))
