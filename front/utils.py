import tensorflow as tf
import math
import numpy as np
import pandas as pd
import os
import shutil
from sklearn import metrics
from tensorflow.python.data import Dataset
from django.conf import settings

tf.logging.set_verbosity(tf.logging.ERROR)

COLS_LIST = [1, 2]
COLUMN_NAME = ['Timestamp', 'Value']
SAVE_PATH = os.path.join(settings.BASE_DIR, 'model_saved')
filename = "model_train"


def first_model_saved(train_data):
    tf.reset_default_graph()
    time_stamp = tf.Variable(train_data["Timestamp"], name="Timestamp")
    value = tf.Variable(train_data["Value"], name="Value")

    tf.add_to_collection('vars', time_stamp)
    tf.add_to_collection('vars', value)

    sess = tf.Session()
    sess.run(tf.global_variables_initializer())
    saver = tf.train.Saver([time_stamp, value])
    saver.save(sess, os.path.join(SAVE_PATH, filename), global_step=1)
    print('First Model Saved')


def update_model_saved(train_data):
    tf.reset_default_graph()
    time_stamp = tf.Variable(train_data["Timestamp"], name="Timestamp")
    value = tf.Variable(train_data["Value"], name="Value")

    tf.add_to_collection('vars', time_stamp)
    tf.add_to_collection('vars', value)

    print('delete old model saved')
    shutil.rmtree(SAVE_PATH)
    sess = tf.Session()
    sess.run(tf.global_variables_initializer())
    saver = tf.train.Saver([time_stamp, value])
    saver.save(sess, os.path.join(SAVE_PATH, filename), global_step=1)
    print('Update Model Saved')


def restore_model_saved(checkpoint_path):
    sess = tf.Session()
    saver = tf.train.import_meta_graph(os.path.join(settings.BASE_DIR, 'model_saved', '{}-1.meta'.format(filename)))
    saver.restore(sess, checkpoint_path)
    graph = tf.get_default_graph()
    print("Model Restored")
    var_time_stamp = graph.get_tensor_by_name('Timestamp:0')
    var_value = graph.get_tensor_by_name('Value:0')
    time_stamp = sess.run(var_time_stamp)
    value = sess.run(var_value)
    data_set = pd.DataFrame({'Timestamp': time_stamp, 'Value': value})
    data_set["Timestamp"] = pd.to_datetime(data_set["Timestamp"].str.decode("utf-8"), errors='coerce').dt. \
        strftime('%Y-%m-%d %H:%M:%S')
    return data_set


def reset_model_save():
    tf.reset_default_graph()
    shutil.rmtree(SAVE_PATH)


def read_train_file_csv(train):
    train_data_frame = pd.read_csv(train, error_bad_lines=False, usecols=COLS_LIST, parse_dates=True,
                                   skipinitialspace=True).sort_values(['Timestamp'])
    train_data_frame.columns = COLUMN_NAME
    train_data_frame["Timestamp"] = pd.to_datetime(train_data_frame["Timestamp"], errors='coerce').dt.\
        strftime('%Y-%m-%d %H:%M:%S')

    checkpoint_path = None

    try:
        checkpoint_path = tf.train.latest_checkpoint('./model_saved')
    except Exception as err:
        print("============ Error:", err)

    if checkpoint_path:
        try:
            print("============ start to restore")
            restore_model_saved(checkpoint_path)
            data_restore = restore_model_saved(checkpoint_path)
            train_data_frame = pd.concat([train_data_frame, data_restore])
        except Exception as err:
            print("============ Error:", err)
        else:
            try:
                print("============ start update model save")
                update_model_saved(train_data_frame)
            except Exception as err:
                print("============ Update model save Error:", err)
    else:
        first_model_saved(train_data_frame)

    return train_data_frame


def preprocess_features(prototype_data_frame):
    selected_features = prototype_data_frame[['Value']]
    processed_features = selected_features.copy()
    processed_features["Value"] = prototype_data_frame['Value']
    return processed_features


def preprocess_targets(prototype_data_frame):
    output_targets = pd.DataFrame()
    output_targets["Value"] = prototype_data_frame["Value"]
    return output_targets


def my_input_fn(features, targets, batch_size=1, shuffle=True, num_epochs=None):
    features = {key: np.array(value) for key, value in dict(features).items()}
    
    ds = Dataset.from_tensor_slices((features, targets))
    ds = ds.batch(batch_size).repeat(num_epochs)
    
    # Shuffle the data, if specified.
    if shuffle:
        ds = ds.shuffle(10000)
    
    # Return the next batch of data.
    features, labels = ds.make_one_shot_iterator().get_next()
    return features, labels


def construct_feature_columns(input_features):
    return set([tf.feature_column.numeric_column(my_feature) for my_feature in input_features])


def train_model(learning_rate, steps, batch_size, training_examples, training_targets, validation_examples,
                validation_targets):
    
    periods = 10
    # periods = len(training_targets)
    steps_per_period = steps / periods
    
    # Create a linear regressor object.
    my_optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)
    my_optimizer = tf.contrib.estimator.clip_gradients_by_norm(my_optimizer, 5.0)
    linear_regressor = tf.estimator.LinearRegressor(
        feature_columns=construct_feature_columns(training_examples),
        optimizer=my_optimizer
    )
    
    # Create input functions.
    training_input_fn = lambda: my_input_fn(
        training_examples,
        training_targets["Value"],
        batch_size=batch_size)
    predict_training_input_fn = lambda: my_input_fn(
        training_examples,
        training_targets["Value"],
        num_epochs=1,
        shuffle=False)
    
    predict_validation_input_fn = lambda: my_input_fn(
        validation_examples, validation_targets["Value"],
        num_epochs=1,
        shuffle=False)
    
    # Train the model, but do so inside a loop so that we can periodically assess
    # loss metrics.
    print("Training model...")
    print("RMSE (on training data):")
    training_rmse = []
    validation_rmse = []
    occasionally_loss = []
    for period in range(0, periods):
        # Train the model, starting from the prior state.
        linear_regressor.train(
            input_fn=training_input_fn,
            steps=steps_per_period,
        )
        # Take a break and compute predictions.
        training_predictions = linear_regressor.predict(input_fn=predict_training_input_fn)
        training_predictions = np.array([item['predictions'][0] for item in training_predictions])
        
        validation_predictions = linear_regressor.predict(input_fn=predict_validation_input_fn)
        validation_predictions = np.array([item['predictions'][0] for item in validation_predictions])
        
        # Compute training and validation loss.
        training_root_mean_squared_error = math.sqrt(
            metrics.mean_squared_error(training_predictions, training_targets))
        validation_root_mean_squared_error = math.sqrt(
            metrics.mean_squared_error(validation_predictions, validation_targets))
        # Occasionally print the current loss.
        occasionally_loss.append("  period %02d : %0.2f" % (period, training_root_mean_squared_error))
        print("  period %02d : %0.2f" % (period, training_root_mean_squared_error))
        # Add the loss metrics from this period to our list.
        training_rmse.append(training_root_mean_squared_error)
        validation_rmse.append(validation_root_mean_squared_error)
    print("Model training finished.")
    
    return linear_regressor, occasionally_loss


def process_predict(train_file, test_file):

    train_data_frame = read_train_file_csv(train_file)

    training_examples = preprocess_features(train_data_frame.head(12000))
    training_examples.describe()

    training_targets = preprocess_targets(train_data_frame.head(12000))
    training_targets.describe()

    validation_examples = preprocess_features(train_data_frame.tail(5000))
    validation_examples.describe()

    validation_targets = preprocess_targets(train_data_frame.tail(5000))
    validation_targets.describe()

    linear_regressor, occasionally_loss = train_model(
        learning_rate=0.00001,
        steps=100,
        batch_size=1,
        training_examples=training_examples,
        training_targets=training_targets,
        validation_examples=validation_examples,
        validation_targets=validation_targets)

    test_data_frame = pd.read_csv(test_file, sep=",", usecols=COLS_LIST, parse_dates=True, skipinitialspace=True).\
        sort_values('Timestamp')
    test_data_frame.columns = COLUMN_NAME
    test_data_frame["Timestamp"] = pd.to_datetime(test_data_frame["Timestamp"], errors='coerce').dt.\
        strftime('%Y-%m-%d %H:%M:%S')

    test_examples = preprocess_features(test_data_frame)
    test_targets = preprocess_targets(test_data_frame)

    predict_test_input_fn = lambda: my_input_fn(
          test_examples,
          test_targets["Value"],
          num_epochs=1,
          shuffle=False)

    print("predict_test_input_fn:", predict_test_input_fn)

    test_predictions = linear_regressor.predict(input_fn=predict_test_input_fn)
    test_predictions = np.array([item['predictions'][0] for item in test_predictions])

    root_mean_squared_error = math.sqrt(metrics.mean_squared_error(test_predictions, test_targets))
    print("Final RMSE (on test data): %0.2f" % root_mean_squared_error)

    return "Final RMSE (on test data): %0.2f" % root_mean_squared_error, occasionally_loss
