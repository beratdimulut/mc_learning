from django import forms


class TensorFlowForm(forms.Form):
    train_file = forms.FileField(required=False)
    predict_file = forms.FileField(required=True)
